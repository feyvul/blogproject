from django.shortcuts import render
from django.views.generic import TemplateView, CreateView


#Class based Views
class ContactView(TemplateView):
    template_name = 'contact.html'

    def get(self, request, *args, **kwargs):
       return super().get(request, *args, **kwargs)

class AboutView(TemplateView):
    template_name = 'about.html'

    def get(self, request, *args, **kwargs):
        print(request.path)
        return super().get(request, *args, **kwargs)


class PostCreateView(CreateView):
    model = posts
    form_class = PostForm
    template_name = 'posts/post_form.html'
    success_url = reverse_lazy('home')

class PostUpdateView(UpdateView):
    model = Posts
    form_class = PostForm
    template_name = 'posts/post_form.html'
    success_url = reverse_lazy('home')

class PostDeleteView(DeleteView):
    model = Posts
    template_name = 'posts/post_confirm_delete.html'
    success_url = reverse_lazy('home')
#
# # Functional Views
# def contact(request):
#     return render(request, 'contact.html')
#
#
# def about(request):
#     return render(request, 'about.html')