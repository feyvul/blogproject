from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import ListView , DetailView, CreateView, UpdateView, DeleteView
from .forms import CommentForm, PostForm
from .models import Posts, FeaturedPost
from django.urls import reverse_lazy


from .models import Posts, FeaturedPost
# functional Based views
def home(request):
    # Sample blog data
    visits_count = request.session.get('visits_count', 0)
    request.session['visits_count'] = visits_count + 1
    posts = Posts.objects.order_by('-date_posted')
    featured_posts = FeaturedPost.objects.all().order_by('priority')
    context = {'posts': posts, 'featured_posts':featured_posts, "count": visits_count}
    return render(request, 'home.html', context)

#class based Views
class HomeView(ListView):
    model = Posts
    template_name = 'home.html'
    context_object_name = 'posts'
    ordering = ['title']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['featured_posts'] = FeaturedPost.objects.all().order_by('priority')
        visits_count = self.request.session.get('visits_count', 0)
        self.request.session['visits_count'] = visits_count + 1
        context['count'] = visits_count
        return context


# class PostDetailView(DetailView):
#     model = Posts
#     template_name = 'posts/post_detail.html'
#
#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['comments'] = self.object.comments.all()
#         return context
#
#     def post(self,request,*args,**kwargs):
#         pdb.set_trace()
#         print(request.POST)
#         self.object = self.get_object()
#         comment_form = CommentForm(request.POST)
#         if comment_form.is_valid():
#             comment = comment_form.save(commit=False)
#             comment.author = request.user
#             comment.post = self.object
#             comment.save()
#             return redirect('post_detail', pk=self.object.id)
#         context = self.get_context_data(object=self.object)
#         context['comment_form'] = comment_form
#         return self.render_to_response(context)

def post_detail(request, pk):
    # Retrieve the Post object
    post = get_object_or_404(Posts, pk=pk)

    # Handle the POST request
    if request.method == 'POST':
        comment_form = CommentForm(request.POST)
        if comment_form.is_valid():
            comment = comment_form.save(commit=False)
            comment.author = request.user
            comment.post = post
            comment.save()
            return redirect('post_detail', pk=post.pk)
    else:
        comment_form = CommentForm()

    # Context data for rendering the template
    context = {
        'post': post,
        'comments': post.comments.all(),
        'comment_form': comment_form
    }

    return render(request, 'posts/post_detail.html', context)

class PostCreateView(CreateView):
    model = Posts
    form_class = PostForm
    template_name = 'posts/post_form.html'
    success_url = reverse_lazy('home')

class PostUpdateView(UpdateView):
    model = Posts
    form_class = PostForm
    template_name = 'posts/post_form.html'
    success_url = reverse_lazy('home')

class PostDeleteView(DeleteView):
    model = Posts
    template_name = 'posts/post_confirm_delete.html'
    success_url = reverse_lazy('home')