from django.urls import path, include

from .views import *
urlpatterns = [
    path('', HomeView.as_view(), name="home"),
    path('post/<int:pk>/', post_detail, name="post_detail"),
    path('post/new/', PostCreateView.as_view(), name="post_create"),
    path('post/<int:pk>/edit/', PostUpdateView.as_view(), name='post_update'),
    path('post/<int:pk>/delete/', PostDeleteView.as_view(), name='post_delete'),
]