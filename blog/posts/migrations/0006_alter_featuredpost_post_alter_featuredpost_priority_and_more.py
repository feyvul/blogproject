# Generated by Django 5.0.6 on 2024-06-16 06:19

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("posts", "0005_alter_comments_post_alter_comments_user_featuredpost"),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.AlterField(
            model_name="featuredpost",
            name="post",
            field=models.OneToOneField(
                on_delete=django.db.models.deletion.CASCADE, to="posts.posts"
            ),
        ),
        migrations.AlterField(
            model_name="featuredpost",
            name="priority",
            field=models.IntegerField(default=0),
        ),
        migrations.CreateModel(
            name="Comment",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("content", models.TextField()),
                ("date_posted", models.DateField(auto_now_add=True)),
                (
                    "author",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
                (
                    "post",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="comments",
                        to="posts.posts",
                    ),
                ),
            ],
        ),
        migrations.DeleteModel(
            name="Comments",
        ),
    ]
