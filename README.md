# Backend Technologies

In this course module you will learn what is Django and how it is used in creating powerful and dynamic web applications. You will experience all key areas required in web application development. From rendering web pages per user request, through processing user input and data, to saving it and retrieving from database.

The module heavily focuses on server side (backend) programming and leaves things like CSS and JS concepts untouched to make the learning experience as easy and understandable as possible.

Some of the topics covered:
- Prequisities
- Creating and configuring Django project
- Introduction to Django
- Django MVC
- Django templates and forms
- Sessions
- Authorization and authentication
- Security
- Django Admin Panel
- Django REST Framework

## Presentation
Presentation can be found [here](https://gitlab.com/sda-international/program/python/16-backend/-/wikis/uploads/9517a1118fb141a36d511dc4fe9b74f7/Backend_Technologies.pdf).


## Further reading:

### Links
- [https://www.djangoproject.com/start/](https://www.djangoproject.com/start/)
- [https://realpython.com/tutorials/django/](https://realpython.com/tutorials/django/)
- [https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django)