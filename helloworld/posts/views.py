from django.shortcuts import render

# Create your views here.
def home(request):
    # Sample blog data
    posts = [
        {
            'title': 'First Blog Post',
            'author': 'John Doe',
            'content': 'This is the content of the first blog post.',
            'date_posted': 'June 1, 2024'
        },
        {
            'title': 'Second Blog Post',
            'author': 'Jane Smith',
            'content': 'This is the content of the second blog post.',
            'date_posted': 'June 2, 2024'
        },
        {
            'title': 'Third Blog Post',
            'author': 'John Snow',
            'content': 'This is the content of the third blog post.',
            'date_posted': 'June 3, 2024'
        }
        # Add more blog entries here if needed
    ]

    context = {'posts': posts}
    return render(request, 'home.html', context)